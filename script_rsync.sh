#!/bin/bash

# current date
dstr=$(date "+%Y-%m-%d %H:%M:%S")

# outfile
OUTFILE="/logdir/storage_dev.log"

# source folder
SRC_F="/src_fld_path/"

# destination folder
DST_F="/dest_fld_path/"

echo >> $OUTFILE
echo $dstr >> $OUTFILE
echo >> $OUTFILE
sudo rsync -zvrah --delete $SRC_F $DST_F >> $OUTFILE 2>&1
echo >> $OUTFILE
echo "----------------------------------------------------" >> $OUTFILE


